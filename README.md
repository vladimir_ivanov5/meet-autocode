# Hello Autocode exercise

Complete the code in [com.epam.rd.autotasks.meetautocode.HelloAutocode](./src/main/java/com/epam/rd/autotasks/meetautocode/HelloAutocode.java).
The program should print `"Hello, Autocode!"` to the standard output.
